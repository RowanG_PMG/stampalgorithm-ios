//
//  ViewController.swift
//  StampAlgorithm
//
//  Created by Rowan Louis Gontier on 26/10/18.
//  Copyright © 2018 Rowan Louis Gontier. All rights reserved.
//

import UIKit
import Charts

class ViewController: UIViewController {

    @IBOutlet weak var scatterView: ScatterChartView!
    @IBOutlet weak var stampView: StampView!
    @IBOutlet weak var stampCodeFeedback: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        stampView.stampDelegate = self
        stampCodeFeedback.text = ""
    }

    func plotTransformedPoints(points: [CGPoint]) {
       
        scatterView.backgroundColor = UIColor.white
        scatterView.leftAxis.axisMinimum = -2
        scatterView.leftAxis.axisMaximum = 10
        
        scatterView.rightAxis.enabled = false
        
        scatterView.xAxis.axisMinimum = -2
        scatterView.xAxis.axisMaximum = 10
        
        let transformedPointsChartData = points.map { (point) -> ChartDataEntry in
            return ChartDataEntry(x: Double(point.x), y: Double(point.y))
        }
        let transformedPointsSet = ScatterChartDataSet(values: transformedPointsChartData, label: "Transformed Points")
        transformedPointsSet.drawValuesEnabled = false
        transformedPointsSet.setColor(UIColor.blue)
        
        let data = ScatterChartData(dataSets: [transformedPointsSet])
        
        scatterView.data = data
 
    }
}

extension ViewController: StampDelegate {
    func onStampError(error: StampError) {
        switch error {
        case .noCodeExists:
            print("No stamp code exists")
        case .invalidStampPattern:
            print("Invalid stamp pattern")
        }
    }
    
    func onStampServerRequestMade() {
         print("Stamp server request made")
    }
    
    func onStampServerResult(_ result: StampResult?) {
        print("Stamp code is: \(result?.receipt)")
    }
    

    func onTouchGestureFailed() {
        print("Touch gesture failed")
    }
    
    func handleTransformedPointSet(pointSet: [CGPoint]) {
        plotTransformedPoints(points: pointSet)
    }
    
}


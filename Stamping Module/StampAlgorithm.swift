//
//  StampAlgorithm.swift
//  StampAlgorithm
//
//  Created by Rowan Louis Gontier on 26/10/18.
//  Copyright © 2018 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import Combinatorics
import simd
import GLKit

typealias DiscreteXYPair = (x: Int, y: Int)
typealias XYPair = (x: CGFloat, y: CGFloat)

class StampAlgorithm {
    typealias PointPairWithDistance = (pointsPair: (point1: CGPoint, point2: CGPoint), distanceBetweenPoints: CGFloat)
  
    let numberGridSlotsEachEaxis = 10
    var axisRange: Double
    var cos45: Double
    
    init() {
        cos45 = cos(45 * Double.pi / 180)
        // Since the diagonal of triangulation markers is normalised to 1, the horizontal and vertical distance between orientation markers is cos 45.
        axisRange = cos45
    }
    
    
    func distanceBetweenPoints(point1: CGPoint, point2: CGPoint) -> CGFloat {
        let xDist = point1.x - point2.x
        let yDist = point1.y - point2.y
        return sqrt(pow(xDist, 2) + pow(yDist, 2))
    }
    
    func checkIfPointNearPointSet(point: CGPoint, pointSet: [CGPoint], nearDistance: CGFloat) -> Bool {
        for pointInSet in pointSet {
            if distanceBetweenPoints(point1: pointInSet, point2: point) <= nearDistance {
                return true
            }
        }
        return false
    }
    
    func combosBetweenTwoPoints(points: [CGPoint]) -> [(point1: CGPoint, point2: CGPoint)] {
        let pointPairs = Combinatorics.combinationsWithoutRepetitionFrom(points, taking: 2)
        let pointPairsTuples = pointPairs.map {
            pair in
            return (point1: pair[0], point2: pair[1])
        }
        return pointPairsTuples
    }
    
    func makePointsPairWithDistanceArray(points: [CGPoint]) ->  [PointPairWithDistance] {
        let pointCombos = combosBetweenTwoPoints(points: points)
        let combosWithDistance = pointCombos.map {
            pointsPair in
            return (pointsPair: pointsPair, distanceBetweenPoints: distanceBetweenPoints(point1: pointsPair.point1, point2: pointsPair.point2))
        }
        return combosWithDistance
    }
    
    func getMidPoint(point1: CGPoint, point2: CGPoint) -> CGPoint {
        let midx = (point1.x + point2.x) / 2
        let midY = (point1.y + point2.y) / 2
        return CGPoint(x: midx, y: midY)
    }
    
    func getMaxDistancePair(pointsPairWithDistanceArray: [PointPairWithDistance]) -> PointPairWithDistance? {
        let itemMax = pointsPairWithDistanceArray.max(by: { (a, b) -> Bool in
            return a.distanceBetweenPoints < b.distanceBetweenPoints
        })
        return itemMax
    }
    
    func scalePoints(points: [CGPoint], scaleFactor: CGFloat) -> [CGPoint] {
        let scaledPoints = points.map { point in
            return scalePoint(point: point, scaleFactor: scaleFactor)
        }
        return scaledPoints
    }
    
    func scalePoint(point: CGPoint, scaleFactor: CGFloat) -> CGPoint {
        return CGPoint(x: point.x * scaleFactor, y: point.y * scaleFactor)
    }
    
    func normalizeDistancePairs(pointsPairWithDistanceArray: [PointPairWithDistance]) -> [PointPairWithDistance] {
        let maxDistancePairWithPoints = getMaxDistancePair(pointsPairWithDistanceArray: pointsPairWithDistanceArray)
        let maxVal = maxDistancePairWithPoints!.distanceBetweenPoints
        let normalized = pointsPairWithDistanceArray.map {
            pointPairWithDistance -> PointPairWithDistance in
            let pointsPair = pointPairWithDistance.pointsPair
            let distanceBetweenPair = pointPairWithDistance.distanceBetweenPoints
            let normalisedPoint1 = scalePoint(point: pointsPair.point1, scaleFactor: 1.0 / maxVal)
            let normalisedPoint2 = scalePoint(point: pointsPair.point2, scaleFactor: 1.0 / maxVal)
            return (pointsPair: (point1: normalisedPoint1, point2: normalisedPoint2), distanceBetweenPoints: distanceBetweenPair / maxVal)
        }
        return normalized
    }
    
    func normalizePoints(points: [CGPoint]) -> [CGPoint] {
        let pointsPairWithDistanceArray = makePointsPairWithDistanceArray(points: points)
        let maxDistance = getMaxDistancePair(pointsPairWithDistanceArray: pointsPairWithDistanceArray)?.distanceBetweenPoints
        if points.count > 0 {
            return scalePoints(points: points, scaleFactor: 1.0 / maxDistance!)
        } else {
            return []
        }
    }
    
    
    func translatePoints(points: [CGPoint], offset: XYPair) -> [CGPoint] {
        let translated = points.map { point in
            return CGPoint(x: point.x + offset.x, y: point.y + offset.y)
        }
        return translated
    }
    
    func degreesToRadians(degrees: CGFloat) -> CGFloat {
        return degrees * CGFloat(Double.pi) / 180
    }
    
    func rotatePoint(point: CGPoint, angleInDegrees: CGFloat) -> CGPoint {
        let positionVector = simd_float3(x: Float(point.x), y: Float(point.y), z: 0)
        let rotationMatrix = makeRotationMatrix(angle: GLKMathDegreesToRadians(Float(angleInDegrees)))
        let rotatedVector = positionVector * rotationMatrix
        return CGPoint(x: CGFloat(rotatedVector.x), y: CGFloat(rotatedVector.y))
    }
    
    func rotateAllPoints(points: [CGPoint], angleInDegrees: CGFloat) -> [CGPoint] {
        return points.map { point in
            rotatePoint(point: point, angleInDegrees: angleInDegrees)
        }
    }
    
    func makeRotationMatrix(angle: Float) -> simd_float3x3 {
        let rows = [
            simd_float3( cos(angle), sin(angle), 0),
            simd_float3(-sin(angle), cos(angle), 0),
            simd_float3( 0,          0,          1)
        ]
        
        return float3x3(rows: rows)
    }
    
    func discretizeValue(value: CGFloat) -> Int {
        let notch = CGFloat(axisRange) / CGFloat(numberGridSlotsEachEaxis)
        for i in (-numberGridSlotsEachEaxis/2 - numberGridSlotsEachEaxis/10)...(numberGridSlotsEachEaxis/2 + numberGridSlotsEachEaxis/10)  { // we extend range each side by 10 % to cover slightly greater continuous values
            let discreteVal = i + numberGridSlotsEachEaxis/2
            if value >= CGFloat(i) * notch && value < CGFloat(i + 1) * notch {
                if discreteVal < 0 {
                    return 0
                }
                if discreteVal > numberGridSlotsEachEaxis - 1 {
                    return numberGridSlotsEachEaxis - 1
                }
                return discreteVal
            }
        }
        return -1
    }
    
    func discretizePoint(point: CGPoint) -> DiscreteXYPair {
        let discretizedValues = [point.x, point.y].map { value in discretizeValue(value: value)
        }
        return (x: discretizedValues[0], y: discretizedValues[1])
    }
    
    func discretizeAllPoints(points: [CGPoint]) -> [DiscreteXYPair] {
        return points.map {point in
            discretizePoint(point: point)
        }
    }
    
    func transformRawPointsToDiscreteKeySet(points: [CGPoint]) -> [DiscreteXYPair] {
        
        //Stages in tranformation: normalized (scaled), translated to center, rotated around center. _NT means Normalised and translated. _NTR means Normalised, translated & rotated.
        if points.count < 2 {
            return []
        }
        
        let pointsPairWithDistanceArray = makePointsPairWithDistanceArray(points: points)
        
        let maxDistancePointsPair = getMaxDistancePair(pointsPairWithDistanceArray: pointsPairWithDistanceArray)
        
        let maxDistance = maxDistancePointsPair!.distanceBetweenPoints
        
        let normalizedPointsPairWithDistance = normalizeDistancePairs(pointsPairWithDistanceArray: pointsPairWithDistanceArray)
        
        let normalisedPoints = scalePoints(points: points, scaleFactor: 1.0 / maxDistance)
        let M1M2normalized = getMaxDistancePair(pointsPairWithDistanceArray: normalizedPointsPairWithDistance)
        
        let midPointNormalized = getMidPoint(point1: M1M2normalized!.pointsPair.point1, point2: M1M2normalized!.pointsPair.point2)
        
        let allPoints_NT = translatePoints(points: normalisedPoints, offset: (-midPointNormalized.x, -midPointNormalized.y))
        let M1M2_NT = translatePoints(points: [M1M2normalized!.pointsPair.point1, M1M2normalized!.pointsPair.point2], offset: (-midPointNormalized.x, -midPointNormalized.y))
        
        let Mx = M1M2_NT[0]
        
        let angleToRotateAllPoints = 45 - atan2(Double(Mx.y), Double(Mx.x)) * 180 / Double.pi // so Mx ends up at top right corner
        let allPoints_NTR_Orientation_1 = rotateAllPoints(points: allPoints_NT, angleInDegrees: CGFloat(angleToRotateAllPoints))
        
        let discretePoints_Orientation_1 = discretizeAllPoints(points: allPoints_NTR_Orientation_1)

        return discretePoints_Orientation_1
    }
    
    func discretePointsAreEqual(pointSet1: [DiscreteXYPair], pointSet2: [DiscreteXYPair]) -> Bool {
        if pointSet1.count == pointSet2.count {
            for i in 0..<pointSet1.count {
                if pointSet1[i].x == pointSet2[i].x && pointSet1[i].y == pointSet2[i].y {
                } else {
                    return false
                }
            }
            return true
        } else {
            return false
        }
    }
    
    func getDataPointsFromAllDiscrete(discretePoints: [DiscreteXYPair]) -> [DiscreteXYPair] {
        let discreteMarkers: [DiscreteXYPair] = [(x: 0,y: 0), (x: numberGridSlotsEachEaxis - 1, y: numberGridSlotsEachEaxis - 1)]
        return discretePoints.filter {point in !discreteMarkers.contains { $0 == point }
        }
    }
    
}

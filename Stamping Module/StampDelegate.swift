//
//  StampDelegate.swift
//  StampAlgorithm
//
//  Created by Rowan Louis Gontier on 26/10/18.
//  Copyright © 2018 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import UIKit

protocol StampDelegate {
    func onStampServerRequestMade()
    func onStampServerResult(_ result: StampResult?)
    func onStampError(error: StampError)
    func onTouchGestureFailed()
    func handleTransformedPointSet(pointSet: [CGPoint])
}

//
//  StampResult.swift
//  StampAlgorithm
//
//  Created by Rowan Louis Gontier on 26/10/18.
//  Copyright © 2018 Rowan Louis Gontier. All rights reserved.
//

import Foundation

enum StampError: Error {
    case noCodeExists
    case invalidStampPattern
}

public struct StampResult {
    public var created: Date!
    public var receipt: String!
    public var secure: Bool!
}

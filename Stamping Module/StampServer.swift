//
//  StampServer.swift
//  StampAlgorithm
//
//  Created by Rowan Louis Gontier on 21/11/18.
//  Copyright © 2018 Rowan Louis Gontier. All rights reserved.
//

import Foundation

class StampServer {
    var allStamps: [(id: String, keys: [[DiscreteXYPair]])] = []
    let stampB = (id: "stampB", keys: [[(x: 6, y: 0), (x: 4, y: 8), (x: 0, y: 9)], [(x: 3, y: 9), (x: 5, y: 1), (x: 9, y: 0)]])
    let stampA = (id: "stampA",keys: [[(x: 7, y: 0), (x: 4, y: 8), (x: 0, y: 9)], [(x: 2, y: 9), (x: 5, y: 1), (x: 9, y: 0)]])
    
    init() {
        allStamps = [stampA, stampB]
    }
    
    func findStampIDInDatabaseSet(discreteKey: [DiscreteXYPair]) -> String? {
        for stamp in allStamps {
            for existingKey in stamp.keys {
                var matchCounter = 0
                for existingPair in existingKey {
                    if discreteKey.contains(where: { $0.x == existingPair.x && $0.y == existingPair.y }) {
                        matchCounter = matchCounter + 1
                    }
                }
                if matchCounter == existingKey.count {
                    return stamp.id
                }
            }
        }
        return nil
    }
    
}

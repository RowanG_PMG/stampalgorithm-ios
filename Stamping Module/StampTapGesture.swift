//
//  MiiStampTapGesture.swift
//  StampAlgorithm
//
//  Created by Rowan Louis Gontier on 26/10/18.
//  Copyright © 2018 Rowan Louis Gontier. All rights reserved.
//

import Foundation
import UIKit

class StampTapGestureRecognizer: UIGestureRecognizer {
    
    open var allTouches: Set<UITouch>?
    var stampDelegate: StampDelegate?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        allTouches = event.allTouches
        if self.state == .possible {
            self.state = .recognized
        }
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        stampDelegate?.onTouchGestureFailed()
        self.state = .failed
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        stampDelegate?.onTouchGestureFailed()
        self.state = .failed
    }
}

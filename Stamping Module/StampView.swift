//
//  StampView.swift
//  StampAlgorithm
//
//  Created by Rowan Louis Gontier on 26/10/18.
//  Copyright © 2018 Rowan Louis Gontier. All rights reserved.
//

import UIKit

class StampView: UIView {
    
   open var stampDelegate: StampDelegate?
    
    let touchCount = 5
    
    override init (frame: CGRect) {
        super.init(frame: frame)
        setupStampDetection()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupStampDetection()
    }
    
    func setupStampDetection() {
        let tapRecognizer = StampTapGestureRecognizer(target: self, action: #selector(StampView.handleStamp(_:)))
        tapRecognizer.stampDelegate = stampDelegate
        addGestureRecognizer(tapRecognizer)
    }
    
    @objc func handleStamp(_ sender: StampTapGestureRecognizer) {
        var curTouchCount: Int? = (sender.allTouches?.count)!
        if (curTouchCount != touchCount) {
            #if targetEnvironment(simulator)
            print("Target is simulator")
            simulateStampIDFetch()
            return
            #else
            curTouchCount = nil
            print("Target is physical device")
            #endif
        }
        
        if let _ = curTouchCount {
            
            var stampPoints = [CGPoint]()
            
            for touch in sender.allTouches! {
                let point = touch.location(in: self)
                stampPoints.append(point)
            }
            
            let stampAlgorithm = StampAlgorithm()
            let allDiscretePoints = stampAlgorithm.transformRawPointsToDiscreteKeySet(points: stampPoints)
            
            let discreteDataPoints = stampAlgorithm.getDataPointsFromAllDiscrete(discretePoints: allDiscretePoints)
            
            print("Data points are: \(discreteDataPoints)")
            
            if discreteDataPoints.count > 0 {
                let singlePointSet = allDiscretePoints.map {xyPair in
                    return CGPoint(x: xyPair.x, y: xyPair.y)
                }
                stampDelegate?.handleTransformedPointSet(pointSet: singlePointSet)
            } else {
                stampDelegate?.onStampError(error: .invalidStampPattern)
                return
            }

            fetchStampID(dataPoints: discreteDataPoints, completionHandler: { stampID in
                if stampID != nil {
                    let stampResult = StampResult(created: Date(), receipt: stampID, secure: false)
                    self.stampDelegate?.onStampServerResult(stampResult)
                } else {
                    self.stampDelegate?.onStampError(error: .noCodeExists)
                }
            })
        }
        
        }
    
    func fetchStampID(dataPoints: [DiscreteXYPair], completionHandler: ((String?) -> Void)? ) {
        // Connect with server to check if data points match a code. Get code or error. Would prefer to get promotion directly, instead of stamp code.
        self.stampDelegate?.onStampServerRequestMade()
        //TBD: Implement actual server request
        let stampName = StampServer().findStampIDInDatabaseSet(discreteKey: dataPoints)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            completionHandler?(stampName)
        }
    }
    
    func simulateStampIDFetch() {
        self.stampDelegate?.onStampServerRequestMade()
        let stampResult = StampResult(created: Date(), receipt: "Simulated-StampA", secure: false)
        self.stampDelegate?.onStampServerResult(stampResult)
    }
    
}
